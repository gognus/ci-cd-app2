<html>
<head>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>
<body>
<div class="pt-4 my-md-5 pt-md-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md">

                <img class="mx-auto d-block" src="https://www.universidadesonline.cl/logos/original/logo-instituto-de-ciencias-tecnologicas-ciisa.png" alt="" width="110" height="110">
                <small class="d-block mb-3 text-muted text-center">Solemne 1 Secci&oacute;n 6</small>
                <small class="d-block mb-3 text-muted text-center">Prof. David Enamorado</small>
                <small class="d-block mb-3 text-muted text-center">Taller de Desarrollo de Aplicaciones Empresariales</small>
            </div>

            <div class="col-6 col-md">
                <h5></h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="#"></a></li>
                    <li><a class="text-muted" href="#"></a></li>
                </ul>
            </div>
            <div class="col-6 col-md">
                <h5></h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="#"></a></li>
                    <li><a class="text-muted" href="#"></a></li>
                </ul>
            </div>
            <div class="col-6 col-md">
                <h5>Integrantes</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="#">Ignacio Navarrete</a></li>
                    <li><a class="text-muted" href="#">Luciano Rojo</a></li>

                </ul>
            </div>
        </div>
    </div>
</div>
</body>
</html>
